provider "aws" {
  region = "us-east-1"
}
terraform {
  backend "s3" {
    bucket = "tfrmapril22"
    key = "tf.tfstate"
    dynamodb_table = "tf1"

  }
}